const mysql = require('mysql');

const db = mysql.createConnection({
    host: process.env.host,
    user: process.env.user,
    password: process.env.password,
    database: process.env.database
});

// Connect
db.connect((err) => {
    if (err) {
        console.log(err)
    } else {
        console.log('MySql Connected...');
    }
});

module.exports = db