const express = require('express');
const app = express();
const fs = require('fs');
require('dotenv').config()

const db = require('./database/connection');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Requiring user Routes
const userRoutes = require('./routes/users');
const taskRoutes = require('./routes/tasks');
const subTaskRoutes = require('./routes/subTasks');
const authenticateUser = require('./routes/authenticateUser');

app.use(authenticateUser);
app.use(userRoutes);
app.use(taskRoutes);
app.use(subTaskRoutes);

app.listen('3000', () => {
    console.log('Server started on port 3000');
});