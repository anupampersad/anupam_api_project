const db = require('../database/connection');

const fetchUserIDTaskID = (req, res, next) => {

    let sql = `SELECT userID, task.taskID from subTask JOIN task on subTask.taskID = task.taskID WHERE subTaskID = ${req.params.subTaskID}`;

    let query = db.query(sql, (err, results) => {
        if (err) {
            throw err
        }

        if (results[0] == undefined) {
            res.status(404).json({ message: "Sub task id does not exist" })
        }
        else {
            req.fetch = results[0];
            next()
        }
    })
}

const fetchUserIDparams = (req, res, next) => {

    let sql = `SELECT userID FROM subTask Right JOIN task on subTask.taskID = task.taskID WHERE task.taskID = ${req.params.taskID} LIMIT 1;`;

    let query = db.query(sql, (err, results) => {
        if (err) {
            throw err
        }

        if (results[0] == undefined) {
            res.status(404).json({ message: "Task id does not exist" })
        }
        else {
            req.fetch = results[0];
            next()
        }
    })
}

const fetchUserIDbody = (req, res, next) => {

    let sql = `SELECT userID FROM subTask Right JOIN task on subTask.taskID = task.taskID WHERE task.taskID = ${req.body.taskID} LIMIT 1;`;

    let query = db.query(sql, (err, results) => {
        if (err) {
            throw err
        }

        if (results[0] == undefined) {
            res.status(404).json({ message: "Task id does not exist" })
        }
        else {
            req.fetch = results[0];
            next()
        }
    })
}


module.exports = { fetchUserIDTaskID, fetchUserIDparams, fetchUserIDbody };