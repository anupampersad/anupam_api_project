const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {

    let token = req.get("authorization");
    // second way to extract token 
    // token = req.headers['authorization']

    // Token is in the form of Bearer token
    // Two ways to extract token
    // 1) token = token.slice(7) -> Because bearer takes6 chars and space 1 char 
    // 2) token = token.split(' ')[1] -> Because Bearer token are space seperated

    if (token) {

        token = token.split(' ')[1]

        jwt.verify(token, process.env.secretKey, (error, user) => {
            if (error) {
                res.json({ message: "Invalid Token" })
            }
            else {
                // console.log(user)
                req.user = user
                next()
            }
        });
    }
    else {
        res.json({
            message: "Access Denied. Unauthorised User!!!"
        })
    }
};

module.exports = auth;