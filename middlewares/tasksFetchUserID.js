const db = require('../database/connection');

const tasksFetchUserID = (req, res, next) => {
    let sql = `SELECT userID FROM task WHERE taskID=${req.params.taskID};`;
    let query = db.query(sql, (err, results) => {

        if (err) {
            throw err
        }

        if (results[0] == undefined) {
            res.status(404).json({ message: "Task id does not exist" })
        }
        else {
            req.fetch = results[0];
            next()
        }
    })
}

module.exports = tasksFetchUserID;