const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const auth = require('../middlewares/auth');

const { fetchUserIDTaskID, fetchUserIDparams, fetchUserIDbody} = require('../middlewares/subTasksFetchUserID');

// Get all tasks along with subtasks for all users
router.get('/subTasks', (req, res) => {

    let sql = `SELECT userName, task.description As 'Task', subTask.description AS 'Sub Task' FROM  user LEFT JOIN task ON user.userID = task.userID LEFT JOIN subTask ON task.taskID = subTask.taskID`;
    let query = db.query(sql, (err, results) => {
        if (err) {
            throw err;
        }
        res.json(results);
    });
})

// Get all tasks along with subtasks for a particular user
router.get('/users/:userID/subTasks', auth, (req, res) => {

    if (req.params.userID == req.user.userID) {

        let sql = `SELECT userName, task.description As 'Task', subTask.description AS 'Sub Task' FROM  user JOIN task ON user.userID = task.userID JOIN subTask ON task.taskID = subTask.taskID WHERE user.userID = ${req.params.userID};`;
        let query = db.query(sql, (err, results) => {

            if (err) {
                throw err;
            }
            if (results.length == 0) {
                res.status(404).json("Could not find any tasks for given user")
            }
            else {
                res.json(results);
            }

        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
})

// Get all subtasks of a task for a user
router.get('/tasks/:taskID/subTasks', auth, fetchUserIDparams, (req, res) => {

    if (req.fetch.userID == req.user.userID) {

        let sql = `SELECT task.description AS 'task', subTask.description AS 'SubTask' FROM task JOIN subTask ON task.taskid = subTask.taskID WHERE subTask.taskID = ${req.params.taskID}`;
        let query = db.query(sql, (err, results) => {
            if (err) {
                throw err;
            }
            if (results.length == 0) {
                res.status(404).json({ message: "No subtasks for given Task Id" })
            }
            else {
                res.send(results);
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
})

// Add a new sub-task in a task for a user
router.post('/subTasks', auth, fetchUserIDbody,(req, res) => {

    if (req.fetch.userID == req.user.userID) {

        let subTask = { ...req.body}

        let sql = 'INSERT INTO subTask SET ?';

        db.query(sql, subTask, (err, results) => {

            if (err) {
                throw err;
            }
            else {
                res.json({message:"Sub Task added successfully"});
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
})

// Get a particular subTask of task for a user
router.get('/subTasks/:subTaskID', auth, fetchUserIDTaskID, (req, res) => {

    if (req.fetch.userID == req.user.userID) {

        let sql = `SELECT task.description AS 'Task' , subTask.description AS 'Sub Task' FROM subTask JOIN task ON task.taskID = subTask.taskID WHERE task.taskID = ${req.fetch.taskID} and subTaskID = ${req.params.subTaskID};`;

        let query = db.query(sql, (err, results) => {
            if (err) {
                throw err;
            }
            res.json(results);

        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
})

// Update a particular subTask for a user
router.put('/subTasks/:subTaskID', auth, fetchUserIDTaskID, (req, res) => {

    if (req.fetch.userID == req.user.userID) {

        ({  description } = req.body)


        let sql = `UPDATE subTask SET  description='${description}' WHERE subTaskID = ${req.params.subTaskID} and taskID = ${req.fetch.taskID}`;
        let query = db.query(sql, (err, results) => {

            if (err) {
                throw err;
            }
            else {
                res.json({ message: "Sub-task updated successfully" });
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
});

// Delete a particular subTask
router.delete('/subTasks/:subTaskID', auth, fetchUserIDTaskID, (req, res) => {

    if (req.fetch.userID == req.user.userID) {

        let sql = `DELETE FROM subTask  WHERE subTaskID = ${req.params.subTaskID} and taskID = ${req.fetch.taskID}`;

        let query = db.query(sql, (err, results) => {

            if (err) {
                throw err
            }
            else {
                res.json({message:"Sub task deleted successfully"});
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
});

module.exports = router;