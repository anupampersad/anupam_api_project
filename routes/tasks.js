const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const auth = require('../middlewares/auth');

const fetchUserID = require('../middlewares/tasksFetchUserID');

// Get all tasks for all users
router.get('/tasks', (req, res) => {

    let sql = 'SELECT userName,description FROM task JOIN user ON task.userID = user.userID ORDER BY userName';
    let query = db.query(sql, (err, results) => {
        if (err) {
            throw err;
        }
        res.json(results);
    });
})

// Get all tasks for a user
router.get('/users/:userID/tasks', auth, (req, res) => {

    if (req.params.userID == req.user.userID) {

        let sql = `SELECT description FROM task WHERE userID = ${req.params.userID} `;

        db.query(sql, (err, results) => {

            if (err) {
                throw err;
            }
            if (results.length == 0) {
                res.json({ message: "No tasks available for given user" })
            }
            else {
                res.send(results);
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
})

// Post a new task for a user 
// Pass the userID in the body and then authenticate with the req.body.userID
router.post('/tasks', auth, (req, res) => {

    if (req.body.userID == req.user.userID) {

        let task = { ...req.body, userID: parseInt(req.body.userID) }

        let sql = 'INSERT INTO task SET ?';

        db.query(sql, task, (err, results) => {

            if (err) {
                throw err;
            }
            else {
                res.json({message:"Task added successfully"});
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
})

// Get a particular task for a user
router.get('/tasks/:taskID', auth, fetchUserID, (req, res) => {

    if (req.fetch.userID == req.user.userID) {

        let sql = `SELECT * FROM task WHERE taskID = ${req.params.taskID} `;

        db.query(sql, (err, results) => {

            if (err) {
                res.json({ message: "Invalid Request" })
            }
            else {
                res.json(results);
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
})

// Update a particular task for a user
router.put('/tasks/:taskID', auth, fetchUserID, (req, res) => {

    if (req.fetch.userID == req.user.userID) {

        ({ description } = req.body)

        let sql = `UPDATE task SET description='${description}' WHERE taskID = ${req.params.taskID} and userID = ${req.fetch.userID}`;
        let query = db.query(sql, (err, results) => {

            if (err) {
                throw err;
            }
            else {
                res.json({ message: "Task Updated successfully" });
            }
        });

    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
});

// Delete a particular task for a user
router.delete('/tasks/:taskID', auth, fetchUserID, (req, res) => {

    if (req.fetch.userID == req.user.userID) {

        let sql = `DELETE FROM task WHERE taskID = ${req.params.taskID} and userID = ${req.fetch.userID}`;

        let query = db.query(sql, (err, results) => {

            if (err) {
                throw err
            }
            else {
                res.json({ message: "Task Deleted successfully" });
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }
});


module.exports = router;