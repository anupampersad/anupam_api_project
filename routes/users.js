const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const bcrypt = require('bcrypt');
const saltRounds = 10;

const auth = require('../middlewares/auth');

// Get all users 
router.get('/users', (req, res) => {

    let sql = 'SELECT userName , email FROM user';
    let query = db.query(sql, (err, results) => {

        if (err) {
            throw err;
        }
        res.json(results);
    });
})

// Get a particular user
router.get('/users/:userID', auth, (req, res) => {

    if (req.params.userID == req.user.userID) {

        let sql = `SELECT * FROM user WHERE userID = ${req.params.userID}`;
        let query = db.query(sql, (err, results) => {

            if (err) {
                throw err;
            }
            else {
                res.json(results);
            }
        })
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }

})

// Add a new user
router.post('/users', async (req, res) => {

    const password = req.body.password;
    const encryptedPassword = await bcrypt.hash(password, saltRounds);

    let post = { ...req.body, password: encryptedPassword };

    let sql = 'INSERT INTO user SET ?';

    db.query(sql, post, (err, result) => {

        if (err) {
            throw err;
        }
        else {
            res.json({message:"User added successfully"});
        }
    });
});

// UPDATE A USER
router.put('/users/:userID', auth, async (req, res) => {

    if (req.params.userID == req.user.userID) {

        ({ userName, password, email } = req.body)

        const encryptedPassword = await bcrypt.hash(password, saltRounds);

        let sql = `UPDATE user SET userName = '${userName}', password = '${encryptedPassword}', email = '${email}'   WHERE userID = ${req.params.userID}`;

        let query = db.query(sql, (err, result) => {

            if (err) {
                throw err;
            }
            else {
                res.json({message:"User Updated Successfully"});
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }

});

// Delete a user
router.delete('/users/:userID', auth, (req, res) => {

    if (req.params.userID == req.user.userID) {

        let sql = `DELETE FROM user WHERE userID = ${req.params.userID}`;

        let query = db.query(sql, (err, result) => {

            if (err) {
                throw err
            }
            else {
                res.json({message:"User deleted successfully"});
            }
        });
    }
    else {
        res.status(401).json({ message: "Unauthorised" })
    }

});

module.exports = router;