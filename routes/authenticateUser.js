const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const bcrypt = require('bcrypt');
const saltRounds = 10;

const jwt = require('jsonwebtoken')

router.post('/user/authenticate', (req, res) => {

    ({ email, password } = req.body)

    let sql = `SELECT * FROM user WHERE email = '${email}' ;`
    let query = db.query(sql, async(err, results) => {
        if (err) {
            throw err;
        }
        if(results.length == 0){
            res.json({message:"Invalid email"})
        }

        // Password is coming from req.body and results[0].password is coming from database after querry
        const match = await bcrypt.compare(password, results[0].password);

        if (match) {
            // res.send('Authentic user');

            results[0].password = undefined;
            //This is because we do not want to send password in jsontoken

            // Once we sign using below step, in 'result : results[0]'  we are passing all the data of the user
            // which is fetched from above using querry except password because that is made undefined
            // Instead of using jwt.sign({ result: results[0] } ,
            // it will be better to sign with above method i.e. jwt.sign({ ... results[0] }
            const jsonToken = jwt.sign({ ...results[0] }, process.env.secretKey, {
                expiresIn: "1h"
            });

            return res.json({
                message: "Login Successful",
                token: jsonToken
            });

        }
        else {
            res.json({ "message": "Invalid User" })
        }
    });
})


module.exports = router;